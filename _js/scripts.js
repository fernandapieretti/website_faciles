/* JS Document
*
* @autor Fernanda Pieretti
* @data nov2015
* @ref scripts.js
*
*/

$(window).load(function(){
	
	//Menu
	$("#menuDropDown").click(function() {
		if ($("#header").hasClass("ativo")) {
			$("#header").removeClass("ativo");
		} else {
			$("#header").addClass("ativo");
		}
	});	
	
	//Sliders	
	$("#carousel").carousel( {
		interval : 6000
	});
	
	$('#slidereventos').flexslider({
		animation: "slide",
		animationLoop: false,
		itemWidth: 280,
		itemMargin: 0,
		directionNav: true,
        controlNav: false,
        start: function (slider) {
            $("#slidereventos").removeClass('loading');
        }
	});
		
	//Navegação
	$("#header a").click(function(event) {
		var $anchor = $(this);
		$('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 55 }, 1500, 'easeInOutExpo');
		$("#header").removeClass("ativo");
		event.preventDefault();
	});
	
	//Form
	$(".form .placehold").focus(function() {
		if ($(this).attr("placeholder") == $(this).val()) {
			$(this).val("");
		}
	}).blur(function() {
		if ($(this).val() == "") {
			$(this).val($(this).attr("placeholder"));
		}
	});
	
});

// Animação
$(function(){
	function onScrollInit( items, trigger ) {
		items.each( function() {
		var osElement = $(this),
			osAnimationClass = osElement.attr('data-os-animation'),
			osAnimationDelay = osElement.attr('data-os-animation-delay');
		  
			osElement.css({
				'-webkit-animation-delay':  osAnimationDelay,
				'-moz-animation-delay':     osAnimationDelay,
				'animation-delay':          osAnimationDelay
			});

			var osTrigger = ( trigger ) ? trigger : osElement;
			
			osTrigger.waypoint(function() {
				osElement.addClass('animated').addClass(osAnimationClass);
				},{
					triggerOnce: true,
					offset: '90%'
			});
		});
	}

	onScrollInit( $('.os-animation') );
	onScrollInit( $('.staggered-animation'), $('.staggered-animation-container') );
});